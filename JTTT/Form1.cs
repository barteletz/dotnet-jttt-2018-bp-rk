﻿using HtmlAgilityPack;
using ScrapySharp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonWykonaj_Click(object sender, EventArgs e)
        {
            Log jtttlog = new Log();
   
            String imageExt = null;
            String imageName = null;
            byte[] imageData;
            MemoryStream stream = new MemoryStream();


            jtttlog.log("Wcisnieto przycisk start");
            try
            { 
                WebClient webClient = new WebClient();
                String data = webClient.DownloadString(textBoxURL.Text);
                jtttlog.log("Pobrano plik HTML ze strony : " + textBoxURL.Text);
                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(data);

                var nodes = htmlDocument.DocumentNode.Descendants("img");

                foreach (HtmlNode node in nodes)
                {
                    foreach (HtmlAttribute htmlAttribute in node.Attributes)
                    {
                        if (htmlAttribute.Value.ToLower().Contains(textBoxTekst.Text.ToLower()))
                        {
                            imageExt = node.GetAttributeValue("src");
                            imageName = node.GetAttributeValue("title");
                            imageData = webClient.DownloadData(imageExt);
                            stream = new MemoryStream(imageData);
                        }
                    }
                }

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.UseDefaultCredentials = false;
                smtpClient.EnableSsl = true;
                smtpClient.Port = 587;
                MailMessage message = new MailMessage();
                MailAddress from = new MailAddress("platformyrkbp@gmail.com", "JTTT");

                message.From = from;
                message.To.Add(textBoxEmail.Text);
                message.Subject = "Temat";
                message.Body = "Witam"; 
                message.Attachments.Add(new Attachment(stream, imageName + ".jpg"));

                smtpClient.Host = "smtp.gmail.com"; 
                smtpClient.Credentials = new System.Net.NetworkCredential("platformyrkbp@gmail.com", "platformy123");
                smtpClient.SendAsync(message, textBoxEmail.Text);
                jtttlog.log("Wyslano wiadomosc");
            }
            catch(Exception ex)
            {
                Console.WriteLine("BLAD");
            }
            

        }
    }
}
